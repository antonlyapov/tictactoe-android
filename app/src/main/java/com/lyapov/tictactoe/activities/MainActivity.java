package com.lyapov.tictactoe.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import com.lyapov.tictactoe.R;
import com.lyapov.tictactoe.enums.Player;
import com.lyapov.tictactoe.views.PullImageView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    /*
        [0, 1, 2]
        [3, 4, 5]
        [6, 7, 8]
     */
    private static final int[][] WINNING_POSITIONS = {
            {0, 1, 2},
            {3, 4, 5},
            {6, 7, 8},
            {0, 3, 6},
            {1, 4, 7},
            {2, 5, 8},
            {0, 4, 8},
            {2, 4, 6},
    };


    private TextView mPlayerOneScoreTextView;
    private TextView mPlayerTwoScoreTextView;
    private GridLayout mGridLayout;


    private Player mActivePlayer = Player.One;

    private List<PullImageView> mPullImageViews = new ArrayList<>();

    private int mPlayerOneScore = 0;
    private int mPlayerTwoScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();
        addImageViews();
    }

    private void initUI() {
        mPlayerOneScoreTextView = findViewById(R.id.tv_player_one_score);
        mPlayerTwoScoreTextView = findViewById(R.id.tv_player_two_score);
        mGridLayout = findViewById(R.id.gv_board);
    }

    private void addImageViews() {
        int rows = mGridLayout.getRowCount();
        int columns = mGridLayout.getColumnCount();

        for (int row = 0; row < rows; row++) {
            for (int column = 0; column < columns; column++) {
                GridLayout.LayoutParams params = new GridLayout.LayoutParams(
                        GridLayout.spec(row), GridLayout.spec(column));

                params.width = (int) (getDimension(R.dimen.activity_main_grid_size) / columns);
                params.height = (int) (getDimension(R.dimen.activity_main_grid_size) / rows);

                int padding = (int) getDimension(R.dimen.activity_main_grid_item_padding);

                PullImageView imageView = new PullImageView(this);
//                imageView.setId(rows * row + column);
                imageView.setTag(rows * row + column);
                imageView.setPadding(padding, padding, padding, padding);
                imageView.setLayoutParams(params);
                imageView.setOnClickListener(this);

                imageView.setIndex(rows * row + column);

                mGridLayout.addView(imageView);
                mPullImageViews.add(imageView);

                log(":addImageViews -> added in GridView ImageView(" + imageView.getId() + ")");
            }
        }
    }

    private float getDimension(int resId) {
        return getResources().getDimension(resId);
    }

    private void log(Object object) {
        Log.i(TAG, object.toString());
    }

    private void switchPlayer() {
        if (mActivePlayer == Player.One) {
            mActivePlayer = Player.Two;
        } else if (mActivePlayer == Player.Two){
            mActivePlayer = Player.One;
        }
    }

    private void checkForWinner() {
        for (int[] winningPosition : WINNING_POSITIONS) {
            PullImageView imageView1 = mGridLayout.findViewWithTag(winningPosition[0]);
            PullImageView imageView2 = mGridLayout.findViewWithTag(winningPosition[1]);
            PullImageView imageView3 = mGridLayout.findViewWithTag(winningPosition[2]);

            if (imageView1.getPlayer() == imageView2.getPlayer() && imageView2.getPlayer() == imageView3.getPlayer()) {
                Player player = imageView1.getPlayer();

                if (player == Player.Default) {
                    return;
                }

                if (player == Player.One) {
                    mPlayerOneScore++;
                } else if (player == Player.Two) {
                    mPlayerTwoScore++;
                }

                updateScoreBoard();
                restartGame(getString(R.string.activity_main_winner_message, getString(player.getNameResId())));

                return;
            }
        }

        boolean allSlotsFilled = true;

        for (PullImageView imageView : mPullImageViews) {
            allSlotsFilled &= imageView.getPlayer() != Player.Default;
        }

        if (allSlotsFilled) {
            restartGame(getString(R.string.activity_main_no_winner));
        }
    }

    private void updateScoreBoard() {
        mPlayerOneScoreTextView.setText(String.valueOf(mPlayerOneScore));
        mPlayerTwoScoreTextView.setText(String.valueOf(mPlayerTwoScore));
    }

    private void restartGame(String message) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton(R.string.activity_main_restart, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        for (PullImageView imageView : mPullImageViews) {
                            imageView.setImageResource(0);
                            imageView.setPlayer(Player.Default);
                        }

                        mActivePlayer = Player.One;
                    }
                })
                .show();
    }

    @Override
    public void onClick(View view) {
        boolean isImageView = view instanceof PullImageView;

        if (!isImageView) {
            return;
        }

        PullImageView imageView = (PullImageView) view;

        if (imageView.getTag() instanceof Player) {
            return;
        }

        imageView.setScaleX(1.3f);
        imageView.setScaleY(1.3f);
        imageView.setAlpha(0.0f);

        imageView.setImageResource(mActivePlayer.getImageResId());
        imageView.setPlayer(mActivePlayer);

        imageView.animate()
                .scaleX(1.0f)
                .scaleY(1.0f)
                .alpha(1.0f)
                .rotation(360f)
                .setDuration(300);

        switchPlayer();
        checkForWinner();
    }
}
