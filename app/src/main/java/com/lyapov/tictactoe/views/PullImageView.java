package com.lyapov.tictactoe.views;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

import com.lyapov.tictactoe.enums.Player;

/*
 *  *  ****************************************************************
 *  *  *                  Developed by Anton Lyapov                   *
 *  *  *                     1st Online Solutions                     *
 *  *  *               http://www.1stonlinesolutions.bg               *
 *  *  *          Copyright by 1st Online Solutions, 01 2018           *
 *  *  ****************************************************************
 */
public class PullImageView extends AppCompatImageView {

    private int mIndex;
    private Player mPlayer;

    public PullImageView(Context context) {
        super(context);
        init();
    }

    public PullImageView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PullImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        mPlayer = Player.Default;
    }

    public void setIndex(int index) {
        mIndex = index;
    }
    public int getIndex() {
        return mIndex;
    }


    public void setPlayer(Player player) {
        mPlayer = player;
    }
    public Player getPlayer() {
        return mPlayer;
    }
}
