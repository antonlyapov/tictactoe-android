package com.lyapov.tictactoe.enums;

import com.lyapov.tictactoe.R;

/*
 *  *  ****************************************************************
 *  *  *                  Developed by Anton Lyapov                   *
 *  *  *                     1st Online Solutions                     *
 *  *  *               http://www.1stonlinesolutions.bg               *
 *  *  *          Copyright by 1st Online Solutions, 01 2018           *
 *  *  ****************************************************************
 */
public enum Player {
    Default(0, 0),
    One(R.string.player_name_red, R.drawable.red),
    Two(R.string.player_name_yellow, R.drawable.yellow);

    private int nameResId;
    private int imageResId;

    Player(int nameResId, int imageResId) {
        this.nameResId = nameResId;
        this.imageResId = imageResId;
    }

    public int getNameResId() {
        return nameResId;
    }

    public int getImageResId() {
        return imageResId;
    }
}
